
# Price-Lab (toy project)

This is a real-time trade analysis engine. It is written in Go following rules of the
[Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html).
I.e. the code is divided into layers of abstraction where each inner layer doesn't know anything
of any outer layer. Arranging a software in such way automagically results in uncoupled components
which can be easily replaced, tested and composed. Moreover business abstractions become independent
of concrete frameworks, dbs, UI toolkits and other implementation details. 

### Trading Data Sources
- [Coinbase.com](https://www.coinbase.com/) ([Websocket Feed](https://docs.pro.coinbase.com/#websocket-feed)).
  
  Currently only Coinbase is supported as a source of trades. However it's only a question of adding
  another implementation of the **TradeSource** abstraction. The business logic will remain intact thanks
  to the Clean Arch. 

### Analysis Tools
- **VWAP** – Volume-Weighted Average Price
- **SMAP** – Simple Moving Average Price
- **SMAV** – Simple Moving Average Volume

In all cases above window size is configurable for both **number** of items and their **age**.

### Available Presenters (UI)
- **text stream** – trades and analysis data get printed into a standard output (one per line)

### Makefile
There is a [Makefile](./Makefile) with convenient targets:

- `$ make lint` – run `golangci-lint`
- `$ make test` – run tests
- `$ make bench` – run benchmarks
- `$ make cover` – check test coverage
- `$ make build` – build an executable

### How to Run
Run `$ make build` to build an executable and then `$ ./bin/price-lab --help` to see available config knobs:
>
```
Usage: price-lab [--max-size MAX-SIZE] [--max-age MAX-AGE] [--vwap] [--smap] [--smav] [SYMBOL [SYMBOL ...]]

Positional arguments:
  SYMBOL                 symbols to watch [default: BTC-USD ETH-USD ETH-BTC]
    
Options:
  --max-size MAX-SIZE    max number of trades in a sliding window [default: 200]
  --max-age MAX-AGE      max age of trades in a sliding window [default: -1ns]
  --vwap                 add VWAP when formatting a trade [default: true]
  --smap                 add SMAP when formatting a trade [default: false]
  --smav                 add SMAV when formatting a trade [default: false]
  --help, -h             display this help and exit
```

**Note:** a user can subscribe to available trading symbols by specifiying them
as positional arguments: `$ bin/price-lab BTC-EUR ETH-EUR`

### Layers of abstraction

- **entities** - this is the most abstract layer - it doesn't depend on any other layer.

  > Packages: **pkg/entity** and **pkg/window** define business data types, interfaces, constants
  > and Enterprise rules.
    
- **usecases** - this is the next level of abstraction – business use cases. Currently there is
  only one such use case – continuous presentation of incoming trading data.

  > Package: **pkg/usecase** defines a func `StreamTradeSource` which accepts abstract dependencies
  > (dependency inversion princile).
    
- **presenters, controllers, gateways, adapters** - this is where concrete implementations of the
  abstract entities are defined. The code here is down to Earth, talking directly to external
  frameworks, toolkits, libraries, etc. Doing all dirty work.

  > Packages: **pkg/source**, **pkg/formatter**, **pkg/presenter**.
    
