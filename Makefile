.DEFAULT_GOAL := test

.PHONY: lint
lint:
	golangci-lint cache clean && golangci-lint run ./... -v --timeout 30s

.PHONY: test
test:
	go test -race -v ./...

.PHONY: bench
bench:
	go test -v -benchmem -bench=. -run=Benchmark ./...

.PHONY: cover
cover:
	go install github.com/ory/go-acc@latest
	go-acc -o coverage.out ./...
	go tool cover -func coverage.out
	go tool cover -html=coverage.out

.PHONY: build
build:
	go build -o bin/price-lab ./cmd/price-lab

