package main

import (
	"strings"
	"time"

	"github.com/alexflint/go-arg"
)

// Config represents an application configuration.
//
type Config struct {
	Symbol        []string      `arg:"positional" help:"symbols to watch [default: BTC-USD ETH-USD ETH-BTC]"`
	MaxWindowSize int           `arg:"--max-size" help:"max number of trades in a sliding window" default:"200"`
	MaxWindowAge  time.Duration `arg:"--max-age" help:"max age of trades in a sliding window" default:"-1ns"`
	WithVWAP      bool          `arg:"--vwap" help:"add VWAP when formatting a trade" default:"true"`
	WithSMAP      bool          `arg:"--smap" help:"add SMAP when formatting a trade" default:"false"`
	WithSMAV      bool          `arg:"--smav" help:"add SMAV when formatting a trade" default:"false"`
}

const defaultSymbols = "BTC-USD,ETH-USD,ETH-BTC"

// GetConfig parses command-line argumens and returns a populated Config struct.
//
func GetConfig() *Config {
	var cfg Config

	arg.MustParse(&cfg)

	if len(cfg.Symbol) == 0 {
		cfg.Symbol = strings.Split(defaultSymbols, ",")
	}

	return &cfg
}
