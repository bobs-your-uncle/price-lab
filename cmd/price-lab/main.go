package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
	"bitbucket.org/bobs-your-uncle/price-lab/pkg/formatter"
	"bitbucket.org/bobs-your-uncle/price-lab/pkg/presenter"
	"bitbucket.org/bobs-your-uncle/price-lab/pkg/source"
	"bitbucket.org/bobs-your-uncle/price-lab/pkg/usecase"
	"bitbucket.org/bobs-your-uncle/price-lab/pkg/window"
)

func main() {
	var logr = log.New(os.Stdout, "", log.Ldate|log.Ltime)

	cfg := GetConfig()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// handle an OS termination signal
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-quit
		logr.Printf("WARN: received an OS signal %v - shutting down", sig)
		cancel()
	}()

	// stream N trade sources
	wg := new(sync.WaitGroup)

	for _, src := range source.NewCoinbaseTradeSources(ctx, cfg.Symbol) {
		fmtr := prepareTradeFormatter(src.Symbol, cfg)
		tprn := presenter.NewFormattingTradePresenter(fmtr, os.Stdout)
		eprn := presenter.NewLoggingErrorPresenter(logr)

		wg.Add(1)

		go usecase.StreamTradeSource(ctx, wg, src, tprn, eprn)
	}

	wg.Wait()
}

func prepareTradeFormatter(symbol string, cfg *Config) entity.TradeFormatter {
	var extra []*formatter.ExtraValue

	for _, e := range []*struct {
		Enabled  bool
		Label    string
		WindowFn func(int, time.Duration, ...*entity.Trade) entity.TradeToScalarFunc
	}{
		{cfg.WithVWAP, "VWAP", window.VWAP},
		{cfg.WithSMAP, "SMAP", window.SMAP},
		{cfg.WithSMAV, "SMAV", window.SMAV},
	} {
		if e.Enabled {
			winFn := e.WindowFn(cfg.MaxWindowSize, cfg.MaxWindowAge)

			extra = append(
				extra,
				&formatter.ExtraValue{Label: e.Label, ScalarFn: winFn},
			)
		}
	}

	return formatter.NewSingleLineTradeFormatter(symbol, extra...)
}
