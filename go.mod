module bitbucket.org/bobs-your-uncle/price-lab

go 1.16

require (
	github.com/aglyzov/log15 v0.0.0-20160114185918-a4e1172343d3 // indirect
	github.com/aglyzov/ws-machine v0.0.0-20170710073850-b233ba80acdd
	github.com/alexflint/go-arg v1.4.2
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/stretchr/testify v1.7.0
	go.uber.org/goleak v1.1.10
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
