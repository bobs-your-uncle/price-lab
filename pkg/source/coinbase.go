package source

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"

	machine "github.com/aglyzov/ws-machine" // this is my own lib from 2013

	lab "bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

const (
	coinbaseURL    = "wss://ws-feed.pro.coinbase.com"
	coinbaseBroker = "Coinbase.com (Websocket feed)"
)

const coninbaseSubReqTmpl = `{
	"type": "subscribe",
    "product_ids": %v,
    "channels": ["matches", "heartbeat"]
}`

type channels struct {
	Trades chan *lab.Trade
	Errors chan error
}

// NewCoinbaseTradeSources connects to a Coinbase Websocket Feed
// (https://docs.pro.coinbase.com/#websocket-feed) subscribing to
// the given symbols.
//
// Returns a number of populated TradeSource structs.
//
// nolint:gocognit,gocyclo,cyclop // a bit complex
func NewCoinbaseTradeSources(ctx context.Context, symbols []string) []*lab.TradeSource {
	var (
		wsm            = machine.New(coinbaseURL, http.Header{})
		sources        = make([]*lab.TradeSource, len(symbols))
		chanMap        = make(map[string]channels, len(symbols))
		symbolsJSON, _ = json.Marshal(symbols)
		subscribeReq   = []byte(fmt.Sprintf(coninbaseSubReqTmpl, string(symbolsJSON)))
	)

	for i, symbol := range symbols {
		var ch = channels{
			Trades: make(chan *lab.Trade),
			Errors: make(chan error),
		}

		chanMap[symbol] = ch

		sources[i] = &lab.TradeSource{
			Broker: coinbaseBroker,
			Symbol: symbol,
			Trades: ch.Trades,
			Errors: ch.Errors,
		}
	}

	go func() {
		defer func() {
			for _, ch := range chanMap {
				close(ch.Trades)
				close(ch.Errors)
			}
		}()
		defer close(wsm.Command)

		var quit = ctx.Done()

	loop:
		for {
			select {
			case <-quit:
				break loop

			case st, ok := <-wsm.Status:
				if !ok {
					break loop
				}

				if st.Error != nil {
					broadcastError(ctx, chanMap, st.Error)
				}

				switch st.State { //nolint:exhaustive // no need to exepct every state
				case machine.CONNECTED:
					select {
					case <-quit:
						break loop
					case wsm.Output <- subscribeReq:
					}

				case machine.DISCONNECTED:
					broadcastError(ctx, chanMap, lab.ErrDisconnected)
				}

			case msg, ok := <-wsm.Input:
				if !ok {
					break loop
				}

				tr, sym, err := decodeCoinbaseTrade(msg)
				if err != nil {
					if sym != "" {
						if ch, ok := chanMap[sym]; ok {
							reportError(ctx, ch.Errors, err)
						}
					} else {
						broadcastError(ctx, chanMap, err)
					}
				}

				if tr != nil {
					if ch, ok := chanMap[sym]; ok {
						select {
						case <-quit:
							break loop
						case ch.Trades <- tr:
						}
					}
				}
			}
		}
	}()

	return sources
}

func reportError(ctx context.Context, errCh chan error, err error) {
	select {
	case errCh <- err:
	case <-ctx.Done():
	}
}

func broadcastError(ctx context.Context, chanMap map[string]channels, err error) {
	var wg = new(sync.WaitGroup)

	wg.Add(len(chanMap))

	for _, ch := range chanMap {
		go func(errCh chan error) {
			reportError(ctx, errCh, err)
			wg.Done()
		}(ch.Errors)
	}

	wg.Wait()
}

func decodeCoinbaseTrade(rawMsg []byte) (*lab.Trade, string, error) {
	type CoinbaseTrade struct {
		Type      string
		ProductID string `json:"product_id"`
		Time      time.Time
		Size      string
		Price     string
		Side      string
	}

	var cbTrade CoinbaseTrade

	if err := json.Unmarshal(rawMsg, &cbTrade); err != nil {
		return nil, "", fmt.Errorf("%v: %w", err, lab.ErrDecoding) //nolint:errorlint // ErrDecoding is base
	}

	if cbTrade.Type != "match" {
		return nil, cbTrade.ProductID, nil
	}

	price, err := strconv.ParseFloat(cbTrade.Price, 64) //nolint:gomnd // 64bit
	if err != nil {
		return nil, cbTrade.ProductID, fmt.Errorf("%v: %w", err, lab.ErrDecoding) //nolint:errorlint // ErrDecoding is base
	}

	volume, err := strconv.ParseFloat(cbTrade.Size, 64) //nolint:gomnd // 64bit
	if err != nil {
		return nil, cbTrade.ProductID, fmt.Errorf("%v: %w", err, lab.ErrDecoding) //nolint:errorlint // ErrDecoding is base
	}

	var side lab.Side

	switch cbTrade.Side {
	case "sell":
		side = lab.Ask
	case "buy":
		side = lab.Bid
	default:
		return nil, cbTrade.ProductID, fmt.Errorf("unknown trade side %v: %w", cbTrade.Side, lab.ErrDecoding)
	}

	return &lab.Trade{
		Price:  price,
		Volume: volume,
		At:     cbTrade.Time,
		Side:   side,
	}, cbTrade.ProductID, nil
}
