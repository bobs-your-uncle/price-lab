package presenter

import (
	"context"
	"io"

	"bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

// FormattingTradePresenter implements the TradePresenter interface by writing a formatted
// text representation of every Trade to an io.Writer.
//
type FormattingTradePresenter struct {
	Formatter entity.TradeFormatter
	Writer    io.Writer
}

// NewFormattingTradePresenter returns an initialized FormattingTradePresenter.
//
func NewFormattingTradePresenter(fmtr entity.TradeFormatter, wr io.Writer) *FormattingTradePresenter {
	return &FormattingTradePresenter{
		Formatter: fmtr,
		Writer:    wr,
	}
}

// PresentTrade implements the TradePresenter interface.
//
func (p *FormattingTradePresenter) PresentTrade(_ context.Context, tr *entity.Trade) error {
	str := p.Formatter.FormatTrade(tr)
	buf := make([]byte, len(str)+1)

	copy(buf, str)
	buf[len(str)] = '\n'

	for len(buf) > 0 {
		num, err := p.Writer.Write(buf)
		if err != nil {
			return err
		}

		buf = buf[num:]
	}

	return nil
}
