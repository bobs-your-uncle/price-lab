package presenter

import (
	"context"

	"bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

// NullTradePresenter implements the TradePresenter interface by doing nothing.
//
type NullTradePresenter struct{}

// NewNullTradePresenter returns an initialized NullTradePresenter.
// Note: you don't need the constructor – it's going to work with a nil pointer the same.
//
func NewNullTradePresenter() *NullTradePresenter {
	return new(NullTradePresenter)
}

func (p *NullTradePresenter) PresentTrade(_ context.Context, _ *entity.Trade) error {
	return nil // noop
}
