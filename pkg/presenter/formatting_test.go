package presenter

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

type StaticTradeFormatter struct {
	Message string
}

func (f *StaticTradeFormatter) FormatTrade(_ *entity.Trade) string {
	return f.Message
}

func TestFormattingTradePresenter(t *testing.T) {
	t.Parallel()

	var (
		ctx     = context.Background()
		builder = new(strings.Builder)
	)

	pr := NewFormattingTradePresenter(
		&StaticTradeFormatter{"test message"},
		builder,
	)

	require.NotNil(t, pr)

	err := pr.PresentTrade(ctx, new(entity.Trade))
	assert.NoError(t, err)

	err = pr.PresentTrade(ctx, new(entity.Trade))
	assert.NoError(t, err)

	assert.Equal(t, "test message\ntest message\n", builder.String())
}
