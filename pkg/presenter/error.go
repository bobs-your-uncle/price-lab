package presenter

import (
	"context"
	"log"
)

// LoggingErrorPresenter implements the ErrorPresenter interface by logging
// every error to a Logger.
//
type LoggingErrorPresenter struct {
	Logger *log.Logger
}

// NewLoggingErrorPresenter returns an initialized LoggingErrorPresenter.
//
func NewLoggingErrorPresenter(l *log.Logger) *LoggingErrorPresenter {
	return &LoggingErrorPresenter{
		Logger: l,
	}
}

// PresentError implements the ErrorPresenter interface.
//
func (p *LoggingErrorPresenter) PresentError(_ context.Context, msg string, err error) error {
	p.Logger.Printf("ERROR %v: %v", msg, err)

	return nil
}
