package formatter

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

func TestSingleLineTradeFormatter(t *testing.T) {
	t.Parallel()

	fmtr := NewSingleLineTradeFormatter("BTC-USD",
		&ExtraValue{"VWAP", func(tr *entity.Trade) float64 { return 12345.678 }},
		&ExtraValue{"SMA", func(tr *entity.Trade) float64 { return 87654.321 }},
	)

	require.NotNil(t, fmtr)

	assert.Equal(
		t,
		"[BTC-USD] ASK     0.0015007 @ 48123.5       (VWAP 12345.678, SMA 87654.321)",
		//nolint:exhaustivestruct // ignoring unused At
		fmtr.FormatTrade(&entity.Trade{Price: 48123.5, Volume: 0.0015007, Side: entity.Ask}),
	)
}
