package formatter

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

// SingleLineTradeFormatter implements the TradeFormatter interface by formatting
// a Trade as a single text line.
//
type SingleLineTradeFormatter struct {
	Symbol      string
	ExtraValues []*ExtraValue
}

// NewSingleLineTradeFormatter rerturns an initialized SingleLineFormatter.
//
func NewSingleLineTradeFormatter(symbol string, extra ...*ExtraValue) *SingleLineTradeFormatter {
	return &SingleLineTradeFormatter{
		Symbol:      symbol,
		ExtraValues: extra,
	}
}

// FormatTrade implements the TradeFormatter interface.
//
func (f *SingleLineTradeFormatter) FormatTrade(tr *entity.Trade) string {
	var (
		price     = strconv.FormatFloat(tr.Price, 'f', -1, 64)  //nolint:gomnd // 64bit
		volume    = strconv.FormatFloat(tr.Volume, 'f', -1, 64) //nolint:gomnd // 64bit
		appendage string
	)

	if num := len(f.ExtraValues); num > 0 {
		builder := new(strings.Builder)
		builder.WriteString(" (")

		for i, extra := range f.ExtraValues {
			builder.WriteString(extra.Label)
			builder.WriteString(" ")
			//nolint:gomnd // 64bit
			builder.WriteString(strconv.FormatFloat(extra.ScalarFn(tr), 'f', -1, 64))

			if i < num-1 {
				builder.WriteString(", ")
			}
		}

		builder.WriteString(")")

		appendage = builder.String()
	}

	return fmt.Sprintf("[%v] %v %13s @ %-13s%v", f.Symbol, tr.Side, volume, price, appendage)
}
