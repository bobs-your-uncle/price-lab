package formatter

import "bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"

// ExtraValue represents a labeled value func to be appended to a text representation.
//
type ExtraValue struct {
	Label    string
	ScalarFn entity.TradeToScalarFunc
}
