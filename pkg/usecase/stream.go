package usecase

import (
	"context"
	"errors"
	"fmt"
	"sync"

	"bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

// StreamTradeSource continuously receives Trades and errors from a TradeSource and
// presents them using corresponding Presenters.
//
func StreamTradeSource(
	ctx context.Context,
	wg *sync.WaitGroup,
	src *entity.TradeSource,
	tprn entity.TradePresenter,
	eprn entity.ErrorPresenter,
) {
	if wg != nil {
		defer wg.Done()
	}

loop:
	for {
		select {
		case <-ctx.Done():
			break loop

		case tr, ok := <-src.Trades:
			if !ok {
				break loop
			}
			if err := tprn.PresentTrade(ctx, tr); err != nil {
				_ = eprn.PresentError(ctx, "failed to present a trade", err)
			}

		case err, ok := <-src.Errors:
			if !ok {
				break loop
			}

			if errors.Is(err, entity.ErrDisconnected) {
				err = fmt.Errorf("%w - reconnecting", err)
			}
			_ = eprn.PresentError(ctx, "trade source reported an error", err)
		}
	}
}
