package entity

// TradeFormatter describes	an object able to format a Trade as a string.
type TradeFormatter interface {
	FormatTrade(tr *Trade) string
}
