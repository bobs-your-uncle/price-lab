package entity

import "context"

// TradePresenter describes an object able to present a Trade in some way.
//
type TradePresenter interface {
	PresentTrade(context.Context, *Trade) error
}
