package entity

import "context"

// ErrorPresenter describes an object able to present an error.
//
type ErrorPresenter interface {
	PresentError(context.Context, string, error) error
}

// ErrDisconnected is a base DISCONNECTED error.
//
const ErrDisconnected = ConstError("disconnected")

// ErrDecoding is a base DECODING error.
//
const ErrDecoding = ConstError("decoding")

// ConstError constructs a special error that is used in *expected* situations
// (this is like `io.EOF` or `sql.ErrNoRows` but can be a *constant*)
//
// Example:
//
//   const ErrNotFound = ConstError("NOT FOUND")
//
//   err := fmt.Errorf("failed to get an item %v: %w", id, ErrNotFound)
//   if errors.Is(err, ErrNotFound) {
//       println("not found!", err)
//   }
//
type ConstError string

func (e ConstError) Error() string {
	return string(e)
}
