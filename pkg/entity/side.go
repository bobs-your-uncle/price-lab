package entity

import "fmt"

// Side represents a side in an order book
//
type Side byte

const (
	// Bid is a side of buyers
	Bid Side = 0
	// Buyer is a side of buyers
	Buyer Side = 0

	// Ask is a side of sellers
	Ask Side = 1
	// Seller is a side of sellers
	Seller Side = 1
)

func (side Side) String() string {
	switch side {
	case Bid:
		return "BID"
	case Ask:
		return "ASK"
	}

	return fmt.Sprintf("UNKNOWN SIDE %v", byte(side))
}
