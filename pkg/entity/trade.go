package entity

import "time"

// Trade represents a single trade (price x volume) at a specific time.
//
type Trade struct {
	Price  float64 // not using big.Float for efficiency
	Volume float64
	At     time.Time
	Side   Side
}

// TradeToScalarFunc is a function that receives a Trade and returns a
// single float64 value.
//
type TradeToScalarFunc func(*Trade) float64 // not using big.Float for efficiency
