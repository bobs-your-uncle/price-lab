package entity

// TradeSource represents a source of Trades.
//
type TradeSource struct {
	// Broker is a name of a Broker.
	Broker string

	// Symbol is a trading instrument name.
	Symbol string

	// Trades is a read-only channel of Trades.
	// The channel gets closed when the source shuts down (due to a context cancelation or
	// an unrecoverable failure).
	//
	Trades <-chan *Trade

	// Errors is a read-only channel of errors.
	// The channel gets closed when the source shuts down (due to a context cancelation or
	// an unrecoverable failure).
	//
	Errors <-chan error
}
