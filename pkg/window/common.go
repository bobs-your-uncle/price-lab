package window

import (
	"container/list"
	"time"

	lab "bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

// filterYoungTrades keeps only young enough Trades preserving relative order.
//
func filterYoungTrades(maxAge time.Duration, trades ...*lab.Trade) []*lab.Trade {
	var (
		lastAt   = trades[len(trades)-1].At
		youngIdx = 0
	)

	for i, s := range trades {
		if lastAt.Sub(s.At) <= maxAge {
			break
		}

		youngIdx = i + 1
	}

	if youngIdx > 0 && youngIdx < len(trades) {
		trades = trades[youngIdx:]
	}

	return trades
}

// pushToWindow adds a new Trade to a window maintaining its limit invariants.
//
func pushToWindow(window *list.List, tr *lab.Trade, maxSize int, maxAge time.Duration, onRemove func(*lab.Trade)) {
	window.PushFront(tr)

	// maintain the size invariant
	for maxSize > 0 && window.Len() > maxSize {
		oldest := window.Remove(window.Back()).(*lab.Trade) // nolint:errcheck,forcetypeassert
		onRemove(oldest)
	}

	// maintain the age invariant
	for maxAge > 0 && window.Len() > 0 {
		var (
			elem   = window.Back()
			oldest = elem.Value.(*lab.Trade)
		)

		if tr.At.Sub(oldest.At) <= maxAge {
			break
		}

		window.Remove(elem)
		onRemove(oldest)
	}
}
