package window //nolint:dupl

import (
	"container/list"
	"time"

	lab "bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

// SMAP produces a TradeToScalarFunc that maintains an internal state
// remembering a limited number of the last input Trades. When called it
// outputs an up-to-date Simple Moving Average (SMA) of a Price.
//
// Parameters:
//
//  - maxSize -- Don't keep more Trades than this [<= 0 means no limit].
//
//  - maxAge  -- Don't keep Trades older than this (relative to the last sample).
//               Note: this assumes the At component of the Trades to monotonically increase.
//			     [<=0 means no limit].
//
//  - seeds   -- initial Trades to seed the internal state [optional].
//
func SMAP(maxSize int, maxAge time.Duration, seeds ...*lab.Trade) lab.TradeToScalarFunc {
	var (
		limited = maxSize > 0 || maxAge > 0
		window  = list.New() // new samples are pushed to Front
		cumP    = 0.0
		count   = 0
	)

	// apply the size limit to the seeds
	if maxSize > 0 && len(seeds) > maxSize {
		seeds = seeds[len(seeds)-maxSize:]
	}

	// apply the age limit to the seeds
	if maxAge > 0 && len(seeds) > 0 {
		seeds = filterYoungTrades(maxAge, seeds...)
	}

	// define the SMA calculation logic
	var addToSMA = func(tr *lab.Trade) float64 {
		cumP += tr.Price
		count++

		return cumP / float64(count)
	}

	var subFromSMA = func(tr *lab.Trade) {
		cumP -= tr.Price
		count--
	}

	// define the main logic
	var updateState = func(tr *lab.Trade) float64 {
		if limited {
			pushToWindow(window, tr, maxSize, maxAge, subFromSMA)
		}

		return addToSMA(tr)
	}

	// seed the initial state
	for _, tr := range seeds {
		updateState(tr)
	}

	return updateState
}
