package window

import (
	"testing"

	"go.uber.org/goleak"
)

func TestMain(m *testing.M) { //nolint:interfacer // expected signature
	goleak.VerifyTestMain(m)
}
