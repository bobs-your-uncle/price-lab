package window

import (
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	lab "bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

type VWAPTestSuite struct {
	suite.Suite
}

func TestVWAP(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(VWAPTestSuite))
}

func (s *VWAPTestSuite) TestZeroVolume() {
	getVWAP := VWAP(-1, -1)

	s.Require().NotNil(getVWAP)

	vwap := getVWAP(&lab.Trade{}) //nolint:exhaustivestruct // zero trade is intended

	s.Equal(0.0, vwap)
}

func (s *VWAPTestSuite) TestUnlimitedWithSeed() {
	seed := []*lab.Trade{
		{Price: 123, Volume: 100},
		{Price: 118, Volume: 5},
		{Price: 119, Volume: 70},
		{Price: 117, Volume: 25},
	}
	getVWAP := VWAP(-1, -1, seed...)

	s.Require().NotNil(getVWAP)

	vwap := getVWAP(&lab.Trade{}) //nolint:exhaustivestruct // zero trade is intended

	s.Equal(120.725, vwap)
}

func (s *VWAPTestSuite) TestSizeLimitedWithSeed() {
	seed := []*lab.Trade{
		{Price: 98, Volume: 84.5}, // ignored on init
		{Price: 100, Volume: 10},  // ignored on init
		{Price: 105, Volume: 150}, // ignored on call
		{Price: 123, Volume: 100},
		{Price: 118, Volume: 5},
		{Price: 119, Volume: 70},
	}
	getVWAP := VWAP(4, -1, seed...) // limited size

	s.Require().NotNil(getVWAP)

	vwap := getVWAP(
		&lab.Trade{Price: 117, Volume: 25}, //nolint:exhaustivestruct // ignoring unused fields
	)

	s.Equal(120.725, vwap)
}

//nolint:dupl
func (s *VWAPTestSuite) TestAgeLimitedWithSeed() {
	past := time.Date(2021, 9, 13, 10, 35, 0, 0, time.UTC)
	seed := []*lab.Trade{
		{Price: 180, Volume: 860, At: past.Add(1 * time.Second)}, // ignored on init
		{Price: 170, Volume: 209, At: past.Add(2 * time.Second)}, // ignored on init
		{Price: 105, Volume: 150, At: past.Add(3 * time.Second)}, // ignored on call
		{Price: 165, Volume: 301, At: past.Add(3 * time.Second)}, // ignored on call
		{Price: 123, Volume: 100, At: past.Add(4 * time.Second)},
		{Price: 118, Volume: 5, At: past.Add(4 * time.Second)},
		{Price: 119, Volume: 70, At: past.Add(7 * time.Second)},
	}
	getVWAP := VWAP(-1, 4*time.Second, seed...) // limited age

	s.Require().NotNil(getVWAP)

	vwap := getVWAP(
		//nolint:exhaustivestruct // ignoring unused fields
		&lab.Trade{Price: 117, Volume: 25, At: past.Add(8 * time.Second)},
	)

	s.Equal(120.725, vwap)
}

func benchmarkVWAP(b *testing.B, maxSize int, maxAge time.Duration, seed, input []*lab.Trade) {
	b.Helper()

	getVWAP := VWAP(maxSize, maxAge, seed...)

	b.ResetTimer()

	for _, tr := range input {
		getVWAP(tr)
	}
}

func BenchmarkVWAPUnlimited(b *testing.B) {
	rndSrc := rand.NewSource(randomSeed)
	trades := generateRandomTrades(rndSrc, b.N)

	benchmarkVWAP(b, -1, -1, nil, trades)
}

func BenchmarkVWAPSizeLimited(b *testing.B) {
	rndSrc := rand.NewSource(randomSeed)
	trades := generateRandomTrades(rndSrc, b.N)

	benchmarkVWAP(b, 50, -1, nil, trades)
}

func BenchmarkVWAPTimeLimited(b *testing.B) {
	rndSrc := rand.NewSource(randomSeed)
	trades := generateRandomTrades(rndSrc, b.N)

	benchmarkVWAP(b, -1, 10*time.Second, nil, trades)
}
