package window //nolint:dupl

import (
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"

	lab "bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

type SMAVTestSuite struct {
	suite.Suite
}

func TestSMAV(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(SMAVTestSuite))
}

func (s *SMAVTestSuite) TestZeroVolume() {
	getSMAV := SMAV(-1, -1)

	s.Require().NotNil(getSMAV)

	smav := getSMAV(&lab.Trade{}) //nolint:exhaustivestruct // zero trade is intended

	s.Equal(0.0, smav)
}

func (s *SMAVTestSuite) TestUnlimitedWithSeed() {
	seed := []*lab.Trade{
		{Price: 123, Volume: 100},
		{Price: 118, Volume: 5},
		{Price: 119, Volume: 70},
	}
	getSMAV := SMAV(-1, -1, seed...)

	s.Require().NotNil(getSMAV)

	smav := getSMAV(
		&lab.Trade{Price: 117, Volume: 25}, //nolint:exhaustivestruct // ignoring unused fields
	)

	s.Equal(50.0, smav)
}

func (s *SMAVTestSuite) TestSizeLimitedWithSeed() {
	seed := []*lab.Trade{
		{Price: 98, Volume: 84.5}, // ignored on init
		{Price: 100, Volume: 10},  // ignored on init
		{Price: 105, Volume: 150}, // ignored on call
		{Price: 123, Volume: 100},
		{Price: 118, Volume: 5},
		{Price: 119, Volume: 70},
	}
	getSMAV := SMAV(4, -1, seed...) // limited size

	s.Require().NotNil(getSMAV)

	smav := getSMAV(
		&lab.Trade{Price: 117, Volume: 25}, //nolint:exhaustivestruct // ignoring unused fields
	)

	s.Equal(50.0, smav)
}

//nolint:dupl
func (s *SMAVTestSuite) TestAgeLimitedWithSeed() {
	past := time.Date(2021, 9, 13, 10, 35, 0, 0, time.UTC)
	seed := []*lab.Trade{
		{Price: 180, Volume: 860, At: past.Add(1 * time.Second)}, // ignored on init
		{Price: 170, Volume: 209, At: past.Add(2 * time.Second)}, // ignored on init
		{Price: 105, Volume: 150, At: past.Add(3 * time.Second)}, // ignored on call
		{Price: 165, Volume: 301, At: past.Add(3 * time.Second)}, // ignored on call
		{Price: 123, Volume: 100, At: past.Add(4 * time.Second)},
		{Price: 118, Volume: 5, At: past.Add(4 * time.Second)},
		{Price: 119, Volume: 70, At: past.Add(7 * time.Second)},
	}
	getSMAV := SMAV(-1, 4*time.Second, seed...) // limited age

	s.Require().NotNil(getSMAV)

	smav := getSMAV(
		//nolint:exhaustivestruct // ignoring unused fields
		&lab.Trade{Price: 117, Volume: 25, At: past.Add(8 * time.Second)},
	)

	s.Equal(50.0, smav)
}

func benchmarkSMAV(b *testing.B, maxSize int, maxAge time.Duration, seed, input []*lab.Trade) {
	b.Helper()

	getSMAV := SMAV(maxSize, maxAge, seed...)

	b.ResetTimer()

	for _, tr := range input {
		getSMAV(tr)
	}
}

func BenchmarkSMAVUnlimited(b *testing.B) {
	rndSrc := rand.NewSource(randomSeed)
	trades := generateRandomTrades(rndSrc, b.N)

	benchmarkSMAV(b, -1, -1, nil, trades)
}

func BenchmarkSMAVSizeLimited(b *testing.B) {
	rndSrc := rand.NewSource(randomSeed)
	trades := generateRandomTrades(rndSrc, b.N)

	benchmarkSMAV(b, 50, -1, nil, trades)
}

func BenchmarkSMAVTimeLimited(b *testing.B) {
	rndSrc := rand.NewSource(randomSeed)
	trades := generateRandomTrades(rndSrc, b.N)

	benchmarkSMAV(b, -1, 10*time.Second, nil, trades)
}
