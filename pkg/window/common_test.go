package window

import (
	"math"
	"math/rand"
	"time"

	lab "bitbucket.org/bobs-your-uncle/price-lab/pkg/entity"
)

const randomSeed = 1234567

func generateRandomTrades(src rand.Source, num int) []*lab.Trade {
	var (
		rnd     = rand.New(src) //nolint:gosec // ok for tests
		trades  = make([]*lab.Trade, num)
		curTime time.Time
	)

	for i := 0; i < num; i++ {
		side := lab.Bid
		if rnd.Int31n(2) == 1 {
			side = lab.Ask
		}

		curTime = curTime.Add(time.Duration(rnd.Int63n(3000)) * time.Millisecond)

		trades[i] = &lab.Trade{
			Price:  math.Abs(rnd.Float64()),
			Volume: math.Abs(rnd.Float64()),
			At:     curTime,
			Side:   side,
		}
	}

	return trades
}
